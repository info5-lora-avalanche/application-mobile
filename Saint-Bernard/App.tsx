import React, { useEffect, useState } from 'react';
import { DatabaseConnectionProvider } from './src/data/connection';
import 'reflect-metadata';
import MyApp from './src/App';
import BluetoothActivationScreen from './src/screens/BluetoothActivation';
import BluetoothSerial from 'react-native-bluetooth-serial';

export default function App() {
  const btManager: BluetoothSerial = BluetoothSerial;
  const [btStatus, setBluetoothStatus] = useState<boolean>(false);

  const getStatusFromDevice = async () => {
    const status = await btManager.isEnabled();
    setBluetoothStatus(status);
  };

  useEffect(() => {
    getStatusFromDevice();
    btManager.on("bluetoothEnabled", () => {
      setBluetoothStatus(true);
    });
    btManager.on("bluetoothDisabled", () => {
      setBluetoothStatus(false);
    });
  }, []);
  return (
    <DatabaseConnectionProvider>
      {btStatus && <MyApp />}
      {!btStatus && <BluetoothActivationScreen />}
    </DatabaseConnectionProvider>
  );
}