import { Image, StyleSheet, Dimensions } from 'react-native';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 16);
const imageWidth = dimensions.width;

const styles = StyleSheet.create({
    logo: {
        width: imageWidth,
        height: imageHeight,
        marginBottom: 10
    },
    onBoardLogo: {
        marginTop:0,
        width: dimensions.width*0.7,
        height: dimensions.width*0.7,
    },
    layout: {
        flex: 1
    },
    icon: {
        width: 42,
        height: 42,
    },
    text: {
        alignItems: 'center'
    },
    drawerContent: {
        flex: 1,
    },
    headerSection: {
        alignItems: 'center',
        marginTop: 15,
    },
    title: {
        fontSize: 16,
        marginTop: 3,
        fontWeight: 'bold',
    },
    section: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15,
    },
    drawerSection: {
        marginTop: 15,
    },
    iconButton: {
        flex: 1,
        justifyContent: 'flex-end',
        bottom: 20,
        alignSelf: 'flex-end'
    },
    twoButtonsContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    flexButton: {
        flex: 1,
    },
    container: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        paddingTop:10,
    },
    bubble: {
        flex: 1,
        backgroundColor: 'rgba(255,255,255,0.7)',
        paddingHorizontal: 18,
        paddingVertical: 12,
        borderRadius: 20,
    },
    buttonContainer: {
        flexDirection: 'row',
        marginVertical: 20,
        backgroundColor: 'transparent',
    },
});

export { styles }