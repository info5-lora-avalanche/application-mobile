import React, { useCallback, useEffect, useState } from 'react';
import { StatusBar } from 'expo-status-bar';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Text } from 'react-native-paper';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';

import { DrawerNavigator } from './navigation/DrawerNavigator';

import { useDatabaseConnection } from './data/connection';

import BluetoothSerial from "react-native-bluetooth-serial";

import HomeScreen from './screens/HomeScreen';
import QRCodeScreen from './screens/QRCodeScreen';
import MapScreen from './screens/MapScreen';
import ListScreen from './screens/ListScreen';
import EditScreen from './screens/EditScreen';
import DetailScreen from './screens/DetailScreen';
import MyDeviceScreen from './screens/MyDeviceScreen';
import OnboardingScreen from './screens/OnboardingScreen';
import { DeviceModel } from './data/entities/DeviceModel';

const Drawer = createDrawerNavigator();
const HomeStack = createStackNavigator();
const QRCodeStack = createStackNavigator();
const MapStack = createStackNavigator();
const ListStack = createStackNavigator();
const MyDeviceStack = createStackNavigator();
const OnboardingStack = createStackNavigator();

const screenOptionsStyle = {
  headerStyle: {
    backgroundColor: '#e10808',
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontWeight: 'bold'
  }
};

const transparentStyle = {
  headerTransparent: true,
  headerTintColor: 'transparent',
  headerTitleStyle: {
    fontWeight: 'bold'
  }
};


const HomeStackScreen = ({ navigation }) => (
  <HomeStack.Navigator screenOptions={screenOptionsStyle}>
    <HomeStack.Screen name="Home" component={HomeScreen} options={{
      title: 'Accueil',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="ios-menu" size={25} backgroundColor="#e10808" onPress={() => navigation.openDrawer()}></Icon.Button>
      )
    }} />
  </HomeStack.Navigator>
);

const QRCodeStackScreen = ({ navigation }) => (
  <QRCodeStack.Navigator screenOptions={screenOptionsStyle}>
    <QRCodeStack.Screen name="Scanner" component={QRCodeScreen} options={{
      title: 'Scanner QR Code',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="ios-menu" size={25} backgroundColor="#e10808" onPress={() => navigation.openDrawer()}></Icon.Button>
      )
    }} />
    <QRCodeStack.Screen name="Create" component={EditScreen} options={{
      title: 'Nouvelle carte',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="arrow-back-outline" size={25} backgroundColor="#e10808" onPress={() => navigation.goBack()}></Icon.Button>
      ),
    }} />
  </QRCodeStack.Navigator>
);

const MapStackScreen = ({ navigation }) => (
  <MapStack.Navigator screenOptions={transparentStyle}>
    <MapStack.Screen name="Map" component={MapScreen} options={{
      title: 'Map',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="ios-menu" size={25} backgroundColor="#e10808" onPress={() => navigation.openDrawer()}></Icon.Button>
      )
    }} />
  </MapStack.Navigator>
);


const ListStackScreen = ({ navigation }) => (
  <ListStack.Navigator screenOptions={screenOptionsStyle}>
    <HomeStack.Screen name="List" component={ListScreen} options={{
      title: 'Listes Cartes Amies',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="ios-menu" size={25} backgroundColor="#e10808" onPress={() => navigation.openDrawer()}></Icon.Button>
      )
    }} />
    <ListStack.Screen name="Detail" component={DetailScreen} options={{
      title: 'Détails carte',
      headerTitleAlign: 'center',
    }} />
    <QRCodeStack.Screen name="Edit" component={EditScreen} options={{
      title: 'Modifier carte',
      headerTitleAlign: 'center',
    }} />
  </ListStack.Navigator>
);

const MyDeviceStackScreen = ({ navigation }) => (
  <MyDeviceStack.Navigator screenOptions={screenOptionsStyle}>
    <MyDeviceStack.Screen name="MyDeviceDetail" component={MyDeviceScreen} options={{
      title: 'Ma carte',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="ios-menu" size={25} backgroundColor="#e10808" onPress={() => navigation.openDrawer()}></Icon.Button>
      )
    }} />
    <MyDeviceStack.Screen name="Edit" component={EditScreen} options={{
      title: 'Modifier carte',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="arrow-back-outline" size={25} backgroundColor="#e10808" onPress={() => navigation.goBack()}></Icon.Button>
      ),
    }} />
  </MyDeviceStack.Navigator>
);

const OnboardingStackScreen = ({ navigation }) => (
  <OnboardingStack.Navigator screenOptions={screenOptionsStyle}>
    <OnboardingStack.Screen name="Onboarding" component={OnboardingScreen} options={{ headerShown: false }} />
    <OnboardingStack.Screen name="Scanner" component={QRCodeScreen} options={{
      title: 'Scanner QR Code',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="ios-menu" size={25} backgroundColor="#e10808" onPress={() => navigation.openDrawer()}></Icon.Button>
      )
    }} />
    <OnboardingStack.Screen name="Create" component={EditScreen} options={{
      title: 'Nouvelle carte',
      headerTitleAlign: 'center',
      headerLeft: () => (
        <Icon.Button name="arrow-back-outline" size={25} backgroundColor="#e10808" onPress={() => navigation.goBack()}></Icon.Button>
      ),
    }} />
  </OnboardingStack.Navigator>
)
export default function MyApp() {
  const [hasDevice, setHasDevice] = useState<DeviceModel | null>(null);
  const [isConnected, setIsConnected] = useState<boolean>(false);
  const [ttc, setTtc] = useState<NodeJS.Timeout | undefined>(undefined);
  const [updateData, setUpdateData] = useState<NodeJS.Timeout | undefined>(undefined);
  const [listFriends, setListFriends] = useState<DeviceModel[]>([]);
  //Connexion à la Base de données
  const { devicesRepository } = useDatabaseConnection();

  useEffect(() => {
    if (ttc === undefined) {
      setTtc(setInterval(async () => {
        if (!isConnected && hasDevice) {
          //console.log("ttc")
          await BluetoothSerial.connect(hasDevice.mac).then(() => setIsConnected(true)).catch(console.error);
        }
      }, 20000));
    }
    setUpdateData(setInterval(async () => {
      if (isConnected && hasDevice) {
        //console.log("updateData : " + hasDevice.longitude + ';' + hasDevice.latitude + ';' + ((hasDevice.signal < -10) ? '2' : '1'))
        await BluetoothSerial.write(hasDevice.longitude + ';' + hasDevice.latitude + ';' + ((hasDevice.signal < -10) ? '2' : '1')).then(() => {console.log('BLE Send')}).catch(console.error)
      }
    }, 20000));
  }, [isConnected, hasDevice])

  BluetoothSerial.withDelimiter("\n").then(res => {
    BluetoothSerial.on("read", mesg => {
      console.log(mesg);
      let message = mesg.data.replace(/[\n\r]/g, '').split(';');
      let value = Number(message[3]) >> 1;
      const newMsg = {
        mac: message[0],
        longitude: message[1],
        latitude: message[2],
        signal: value
      };
      listFriends.forEach(element => {
        if(element.mac == newMsg.mac)
          devicesRepository.updateInfo(newMsg.mac,newMsg.latitude,newMsg.longitude,newMsg.signal);
      });
    });
  });

  BluetoothSerial.on("connectionLost", () => {
    console.log("Connection lost");
    setIsConnected(false);
  });

  BluetoothSerial.on("connectionSuccess", () => {
    console.log("Connection Successful");
    setIsConnected(true);
  });

  useEffect(() => {
    (async () => {
      const device = await devicesRepository.getMyDevice();
      if (device) {
        setHasDevice(device)
        await BluetoothSerial.connect(device.mac).catch(console.error);
        setListFriends(await devicesRepository.getAllFriends());
      } else
        setHasDevice(null)
    })();
  }, []);

  return (
    <NavigationContainer>
      <Drawer.Navigator drawerContent={props => <DrawerNavigator {...props} />}>
        {hasDevice == null && <Drawer.Screen name="Onboarding" component={OnboardingStackScreen} options={{ swipeEnabled: false }} />}
        <Drawer.Screen name="Home" component={HomeStackScreen} />
        <Drawer.Screen name="QRCode" component={QRCodeStackScreen} />
        <Drawer.Screen name="Map" component={MapStackScreen} />
        <Drawer.Screen name="List" component={ListStackScreen} />
        <Drawer.Screen name="MyDeviceDetail" component={MyDeviceStackScreen} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
