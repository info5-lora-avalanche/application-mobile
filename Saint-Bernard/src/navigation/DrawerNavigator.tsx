import React from 'react';
import { View } from 'react-native';
import { styles } from '../../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import {
    Avatar,
    Title,
    Drawer,
    Text
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

export function DrawerNavigator(props) {
    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.headerSection}>
                        <Avatar.Image
                            source={require('../assets/icon.png')}
                            size={100}
                        />
                        <View style={{ marginLeft: 10, marginRight: 10, flexDirection: 'column' }}>
                            <Title style={styles.title}>On garde une patte sur vous !</Title>
                        </View>
                    </View>
                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon
                                    name="home"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Accueil"
                            onPress={() => { props.navigation.navigate('Home') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon2
                                    name="qrcode-scan"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Scanner QRCode"
                            onPress={() => { props.navigation.navigate('QRCode', { screen: 'Scanner', params: { isFriend: true } })}}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon
                                    name="map"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Map"
                            onPress={() => { props.navigation.navigate('Map') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon
                                    name="people"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Liste Cartes Amies"
                            onPress={() => { props.navigation.navigate('List') }}
                        />
                        <DrawerItem
                            icon={({ color, size }) => (
                                <Icon
                                    name="settings-outline"
                                    color={color}
                                    size={size}
                                />
                            )}
                            label="Ma Carte"
                            onPress={() => { props.navigation.navigate('MyDeviceDetail') }}
                        />
                    </Drawer.Section>
                </View>
            </DrawerContentScrollView>
        </View>
    );
}
