import React, { useCallback, useEffect, useState } from "react";
import { Dimensions, RefreshControl, ScrollView, View } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { useDatabaseConnection } from '../data/connection';
import { DeviceModel } from '../data/entities/DeviceModel';
import { QRCode } from 'react-native-custom-qr-codes-expo';
import Icon from 'react-native-vector-icons/Ionicons';
import BluetoothSerial from "react-native-bluetooth-serial";

import { styles } from '../../styles';
import { Colors, TextInput, ActivityIndicator, Text } from "react-native-paper";

function MyDeviceScreen({ navigation }) {
    //Connexion à la Base de données
    const { devicesRepository } = useDatabaseConnection();
    const [device, setDevice] = useState<DeviceModel | null>(null);
    const [status, setStatus] = useState<boolean>(false);

    useFocusEffect(
        useCallback(() => {
            (async () => {
                const bddevice = await devicesRepository.getMyDevice();
                if (bddevice) {
                    setDevice(bddevice);
                    BluetoothSerial.isConnected().then(setStatus);
                    navigation.setOptions({
                        headerRight: () => (
                            <Icon.Button name="pencil" size={25} backgroundColor="#e10808" onPress={() => navigation.push('Edit', { device: bddevice })} />
                        ),
                    });
                    await BluetoothSerial.isConnected().then(setStatus);
                }
            })();
        }, [navigation])
    );

    if (device == null)
        return <ActivityIndicator animating={true} color={Colors.red800} />

    return (
        <ScrollView
            style={[styles.layout, { paddingBottom: 20 }]}
        >
            <View style={{ alignItems: "center" }}>
                <QRCode size={(Dimensions.get('window').width) * 1} content={JSON.stringify({ "MAC": device.mac, "code": device.code })} color={Colors.red500} logo={require('../assets/adaptive-icon.png')} />
                <Text style={{ color: status ? 'green' : 'red' }}>Statut : {status ? 'Connecté' : 'Déconnecté'}</Text>
            </View>
            <TextInput
                label="Nom"
                value={device.nom}
                disabled={true}
            />
            <TextInput
                label="MAC"
                value={device.mac}
                disabled={true}
            />
            <TextInput
                label="Commentaires"
                value={device.commentaire}
                multiline={true}
                disabled={true}
            />
        </ScrollView>
    );
}

export default MyDeviceScreen;