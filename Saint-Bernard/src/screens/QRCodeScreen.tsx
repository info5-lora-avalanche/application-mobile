import React, { useState, useEffect, useLayoutEffect } from "react";
import { Dimensions, View, StyleSheet } from 'react-native';
import { ActivityIndicator, Button, Colors, Text } from 'react-native-paper';
import { BarCodeScanner, BarCodeScannerResult } from 'expo-barcode-scanner';
import { styles } from '../../styles';
import Icon from 'react-native-vector-icons/Ionicons';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import BarcodeMask from 'react-native-barcode-mask';

import { useDatabaseConnection } from '../data/connection';

//Constantes pour la taille du Scanner
const finderWidth: number = 280;
const finderHeight: number = 230;
const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const viewMinX = (width - finderWidth) / 2;
const viewMinY = (height - finderHeight) / 2;

interface Device {
    nom?: string;
    mac: string;
    code: string;
}

function QRScreen({ navigation, route }) {
    const [hasPermission, setHasPermission] = useState<boolean | null>(null);
    const [type, setType] = useState<any>(BarCodeScanner.Constants.Type.back);
    const [scanned, setScanned] = useState<boolean>(false);
    const [device, setDevice] = useState<Device | null>(null);


    const isRegisteringFriend = route.params.isFriend || false;

    //Header
    useLayoutEffect(() => {
        if (!isRegisteringFriend)
            navigation.setOptions({
                headerLeft: () => (
                    <Icon.Button name="arrow-back-outline" size={25} backgroundColor="#e10808" onPress={() => navigation.goBack()}></Icon.Button>
                ),
            });
    }, [navigation]);
    //Regexp pour les adresses MAC
    const macAddr = /^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$/gi;

    //Base de données
    const { devicesRepository } = useDatabaseConnection();

    //Demande des permissions d'accès à la caméra
    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    //Sauvegarde de l'Appareil
    useEffect(() => {
        if (device != null)
            navigation.push('Create', { device: device, isFriend: isRegisteringFriend, isCreating: true });
    }, [device]);

    //Gestion du résultat du scan
    const handleBarCodeScanned = (scanningResult: BarCodeScannerResult) => {
        if (!scanned) {
            const { type, data, bounds: { origin } = {} } = scanningResult;
            // @ts-ignore
            const { x, y } = origin;
            if (x >= viewMinX && y >= viewMinY && x <= (viewMinX + finderWidth / 2) && y <= (viewMinY + finderHeight / 2)) {
                setScanned(true);
                try {
                    let qrdata = JSON.parse(data);
                    if (qrdata.hasOwnProperty('MAC') && macAddr.test(qrdata['MAC']) && qrdata.hasOwnProperty('code') && qrdata.code.length === 4) {
                        setDevice({ mac: qrdata['MAC'], code: qrdata.code })
                    } else
                        throw new Error('Invalid Object format');
                } catch (e) {
                    alert(`QRCode invalide, merci de vérifier celui que vous essayez de scanner.`);
                }
            }
        }
    };

    //Si l'on a pas les permissions, on affiche un texte
    if (hasPermission === null) {
        return (
            <View style={styles.layout}>
                <Text>Demande d'accès à l'appareil photo</Text>
                <ActivityIndicator animating={true} color={Colors.red800} />
            </View>
        );
    }
    if (hasPermission === false) {
        return <Text>Pas d'accès à la caméra</Text>;
    }

    return (
        <View style={styles.layout}>
            <BarCodeScanner onBarCodeScanned={handleBarCodeScanned}
                type={type}
                barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr]}
                style={[StyleSheet.absoluteFillObject]}
            >
                <View
                    style={{
                        flex: 1,
                        backgroundColor: 'transparent',
                        flexDirection: 'column',
                    }} >
                    <BarcodeMask showAnimatedLine={false} />
                </View>
                {scanned && <Button onPress={() => setScanned(false)} mode="contained"
                    icon={({ color, size }) => (
                        <Icon2
                            name='flip-camera-android'
                            color={color}
                            size={size}
                        />)}
                    color="red"
                >Scanner à nouveau</Button>}
            </BarCodeScanner>
        </View>
    );
}
export default QRScreen;