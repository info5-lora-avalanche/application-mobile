import React, { useEffect, useState } from "react";
import { View, Image, TouchableHighlight } from 'react-native';
import { Text, Button, Colors } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import Onboarding from 'react-native-onboarding-swiper';
import { useFocusEffect } from '@react-navigation/native';
import { NavigationAction, StackActions } from '@react-navigation/native';
import { styles } from '../../styles';
import { useDatabaseConnection } from '../data/connection';

function OnboardingScreen({ navigation }) {
    const [hasDevice, setHasDevice] = useState<boolean>(false);
    const { devicesRepository } = useDatabaseConnection();

    useFocusEffect(() => {
        (async () => {
            if (!hasDevice) {
                const device = await devicesRepository.getMyDevice();
                if (device)
                    setHasDevice(true)
                else
                    setHasDevice(false)
            }
        })();
    }
    );
    return (
        <Onboarding
            showSkip={false}
            showDone={hasDevice}
            containerStyles={{ justifyContent: "flex-start" }}
            onDone={() => navigation.reset({ routes: [{ name: 'Home' }] })}
            pages={
                [
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('../assets/mountain.png')} style={styles.onBoardLogo} />,
                        title: 'Bienvenue !',
                        subtitle: 'Vous venez de démarrer Saint-Bernard, votre meilleur ami lors de vos sorties montagnes.',
                    },
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('../assets/casque.png')} style={styles.onBoardLogo} />,
                        title: 'Créez votre réseau',
                        subtitle: 'Ajoutez vos amis, et soyez prévenu en temps réel de leur état de danger.',
                    },
                    {
                        backgroundColor: '#fff',
                        image: hasDevice ? <Image source={require('../assets/stbernard.png')} style={styles.onBoardLogo} /> : <View><Image source={require('../assets/stbernard.png')} style={styles.onBoardLogo} /><Button onPress={() => navigation.push('Scanner', { isFriend: false })} mode="contained" color={Colors.blue500}>Enregistrer ma carte</Button></View>,
                        title: hasDevice ? 'On garde une patte sur vous !' : 'On Commence ?',
                        subtitle: hasDevice ? 'La configuration est terminée, merci d\'utiliser Saint-Bernard !' : 'Pour pouvoir utiliser l\'application, vous devez enregistrer votre carte. Cela permettra de vous localiser en cas de danger.',
                    },
                ]}
        />
    );
}

export default OnboardingScreen;