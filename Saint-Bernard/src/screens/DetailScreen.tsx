import React, { useState, useEffect } from "react";
import { Image, View, StyleSheet, Dimensions, ScrollView } from 'react-native';
import { Avatar, Button, Colors, Text, TextInput } from 'react-native-paper';
import { styles } from '../../styles';
import { QRCode } from 'react-native-custom-qr-codes-expo';

function DetailScreen({ navigation, route }) {

    interface Device {
        id?: number;
        nom: string;
        mac: string;
        code: string;
        commentaire: string;
    }
    const [device, setDevice] = useState<Device>(route.params.device);

    return (
        <ScrollView style={[styles.layout, {paddingBottom: 20}]}>
            <View style={{alignItems: "center"}}>
                <QRCode size={(Dimensions.get('window').width) * 1} content={JSON.stringify({ "MAC": device.mac, "code": device.code })} color={Colors.red500} logo={require('../assets/adaptive-icon.png')} />
            </View>
            <TextInput
                label="Nom"
                value={device.nom}
                disabled={true}
            />
            <TextInput
                label="MAC"
                value={device?.mac}
                disabled={true}
            />
            <TextInput
                label="Commentaires"
                value={device.commentaire}
                multiline={true}
                disabled={true}
            />
        </ScrollView>
    );
}

export default DetailScreen;