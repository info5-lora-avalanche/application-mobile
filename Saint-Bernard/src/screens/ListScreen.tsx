import React, { useCallback, useEffect, useState } from "react";
import { useFocusEffect } from '@react-navigation/native';
import { RefreshControl, ScrollView, View } from 'react-native';
import { ActivityIndicator, Colors, IconButton, List, Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { useDatabaseConnection } from '../data/connection';
import { styles } from '../../styles';


interface Device {
    id: number;
    nom: string;
    mac: string;
    code: string;
    commentaire: string;
}

function ListScreen({ navigation }) {
    //Connexion à la Base de données
    const { devicesRepository } = useDatabaseConnection();

    const [devices, setDevices] = useState<Device[]>([]);

    const handleDeleteDevice = useCallback(
        async (id: number) => {
            await devicesRepository.delete(id);
            setDevices(current => current.filter(device => device.id !== id));
        },
        [devicesRepository],
    );

    const handleEditDevice = function (device: Device) {
        navigation.push('Edit', { device: device, isFriend: true, isCreating: false });
    }

    //On récupère tous les appareils enregistrés dans la Base de données
    useFocusEffect(
        useCallback(() => {
            (async () => {
                devicesRepository.getAllFriends().then(setDevices);
            })();
        }, [navigation])
      );

    if (devices.length == 0)
        return <ActivityIndicator animating={true} color={Colors.red800} />

    return (
        <ScrollView
            style={styles.layout}
        >
            {devices.map(device => (
                <List.Item
                    key={String(device.id)}
                    title={device.nom}
                    description={device.mac}
                    onPress={() => navigation.navigate('Detail', { device: device })}
                    right={props => <View style={{ flexDirection: "row" }}>
                        <IconButton
                            icon="pencil"
                            color={Colors.blue500}
                            size={20}
                            onPress={() => handleEditDevice(device)}
                        />
                        <IconButton
                            icon="delete"
                            color={Colors.red500}
                            size={20}
                            onPress={() => handleDeleteDevice(device.id)}
                        /></View>}
                />
            ))}
        </ScrollView>
    );
}

export default ListScreen;