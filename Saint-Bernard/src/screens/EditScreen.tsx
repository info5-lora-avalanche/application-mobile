import React, { useLayoutEffect, useState } from "react";
import { Image, View, KeyboardAvoidingView, ScrollView, SafeAreaView } from 'react-native';
import { StackActions } from '@react-navigation/native';
import { Button, Colors, TextInput } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';

import { styles } from '../../styles';
import { useDatabaseConnection } from '../data/connection';

import BluetoothSerial from "react-native-bluetooth-serial";

function EditScreen({ navigation, route }) {
    interface Device {
        id?: number;
        nom: string;
        mac: string;
        code: string;
        commentaire: string;
        latitude: string;
        longitude: string;
    }

    const isCreating = route.params.isCreating || false;
    const isFriend = route.params.isFriend || false;

    const [device, setDevice] = useState<Device>(route.params.device);

    const [nom, setNom] = useState<string>(route.params.device.nom || '')
    const [commentaire, setCommentaire] = useState<string>(route.params.device.commentaire || '');

    // Version de developpement : l'utilisateur rentre les coordonnées lui-même
    const [latitude, setLatitude] = useState<string>(route.params.device.latitude || '')
    const [longitude, setLongitude] = useState<string>(route.params.device.longitude || '')
    //

    //Base de données
    const { devicesRepository } = useDatabaseConnection();

    const handleCreateDevice = async () => {
        if (isCreating) {
            if (isFriend) {
                await devicesRepository.create({ nom: nom, mac: device.mac, code: device.code, commentaire: commentaire, latitude: latitude, longitude: longitude })
                    .then((device) => {
                        alert(`L'appareil ${device.mac} a été ajouté !`);
                        navigation.goBack();
                    })
                    .catch(e => {
                        if (e.message.includes("UNIQUE"))
                            alert(`Erreur : Cette carte a déjà été enregistrée dans l'application.`);

                        else
                            alert(`Erreur : ${e}`);
                    });
            } else {
                let error = false;
                await devicesRepository.saveMyDevice({ nom: nom, mac: device.mac, code: device.code, commentaire: commentaire })
                    .catch(e => {
                        error = true;
                        if (e.message.includes("UNIQUE"))
                            alert(`Erreur : Cette carte a déjà été enregistrée dans l'application.`);

                        else
                            alert(`Erreur : ${e}`);
                    });
                if (!error) {
                    //On vérifie si la carte est déjà appareillé au téléphone
                    var listdevices = [];
                    await BluetoothSerial.list().then((res) => listdevices = res);

                    //Si ce n'est pas le cas on lance le processus
                    if (!listdevices.map(x => x.address).includes(device.mac))
                        await BluetoothSerial.pairDeviceAutomatically(device.mac, Number(device.code))
                            .then((paired: boolean) => {
                                if (paired) {
                                    alert(`L'appareil a été ajouté, l'appairage Bluetooth a réussi!`);
                                    navigation.dispatch(StackActions.popToTop());
                                } else {
                                    alert(`Erreur : L'apparaige Bluetooth a échoué.`);
                                }
                            })
                            .catch((err) => console.error(err.message));
                    //Sinon on valide l'enregistrement
                    else {
                        alert(`L'appareil a été ajouté, l'appareil Bluetooth était déjà appairé.`);
                        navigation.dispatch(StackActions.popToTop());
                    }
                }
            }
        } else {
            await devicesRepository.rename(device.id, nom, commentaire)
                .then(() => {
                    alert(`L'appareil ${device.mac} a été mis à jour !`);
                    navigation.goBack();
                })
                .catch(e => {
                    alert(`Erreur : ${e}`);
                });
        }
    };

    return (
        <SafeAreaView>
            <KeyboardAvoidingView behavior="padding" enabled>
                <ScrollView>
                    <Image
                        style={styles.logo}
                        source={require('../assets/adaptive-icon.png')}
                    />
                    <TextInput
                        label="Nom"
                        value={nom}
                        selectionColor={Colors.red500}
                        underlineColor={Colors.red500}
                        onChangeText={text => setNom(text)}
                    />
                    <TextInput
                        label="MAC"
                        value={device.mac}
                        disabled={true}
                    />
                    {isFriend &&
                        <TextInput
                            label="Latitude"
                            value={latitude}
                            selectionColor={Colors.red500}
                            underlineColor={Colors.red500}
                            onChangeText={text => setLatitude(text)}
                        //disabled={true} //si on se met à les recevoir, commenté pour l'instant pour la démo
                        />
                    }
                    {isFriend &&
                        <TextInput
                            label="Longitude"
                            value={longitude}
                            selectionColor={Colors.red500}
                            underlineColor={Colors.red500}
                            onChangeText={text => setLongitude(text)}
                        //disabled={true} si on se met à les recevoir, commenté pour l'instant pour la démo
                        />}
                    <TextInput
                        label="Commentaires"
                        value={commentaire}
                        multiline={true}
                        selectionColor={Colors.red500}
                        underlineColor={Colors.red500}
                        onChangeText={text => setCommentaire(text)}
                    />
                    <View style={styles.twoButtonsContainer}>
                        <Button
                            mode="contained"
                            style={styles.flexButton}
                            color={Colors.blue500}
                            onPress={() => handleCreateDevice()}
                        >
                            {isCreating ? 'Ajouter' : 'Modifier'}
                        </Button>
                        <Button
                            mode="contained"
                            style={styles.flexButton}
                            color={Colors.red500}
                            onPress={() => navigation.goBack()}
                        >
                            Annuler
                </Button>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        </SafeAreaView>
    );
}

export default EditScreen;
