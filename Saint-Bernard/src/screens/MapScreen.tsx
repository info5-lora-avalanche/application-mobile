import React, {
    useCallback,
    useState,
    useRef,
    useEffect,
} from 'react';
import { View, Text } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import MapView, { Marker, PROVIDER_OSMDROID, UrlTile, MAP_TYPES } from 'react-native-maps-osmdroid';
import * as Location from 'expo-location';
import { useDatabaseConnection } from '../data/connection';
import { styles } from '../../styles';

interface Device {
    id: number;
    nom: string;
    mac: string;
    code: string;
    commentaire: string;
    latitude: string;
    longitude: string;
}

export function MapScreen({ navigation }) {
    //Connexion à la Base de données
    const { devicesRepository } = useDatabaseConnection();

    const [devices, setDevices] = useState<Device[]>([]);

    //Région d'origine = centrée sur Grenoble
    const region = {
        latitude: 45.18,
        longitude: 5.72,
        latitudeDelta: 20.0922,
        longitudeDelta: 10.0421
    };

    const [location, setLocation] = useState(null);
    const [errorMsg, setErrorMsg] = useState(null);
    const [text, setText] = useState('Waiting..');

    const [isMapReady, setMapReady] = useState(false);

    const mapRef = useRef(null);

    const handleMapReady = useCallback(() => {
        setMapReady(true);
    }, [mapRef, setMapReady]);

    //On récupère tous les appareils enregistrés dans la Base de données
    useFocusEffect(
        useCallback(() => {
            (async () => {
                let location = await Location.getCurrentPositionAsync({}).catch(console.error);
                await devicesRepository.updateLocation(0, location.coords.latitude.toFixed(4), location.coords.longitude.toFixed(4));
                await devicesRepository.getAll().then(setDevices);
                let { status } = await Location.requestPermissionsAsync();
                if (status !== 'granted') {
                    setErrorMsg('Permission to access location was denied');
                    return;
                }
                setLocation(location);
                
                setText(`latitude: ${JSON.stringify(location.coords.latitude.toFixed(4))}, longitude: ${JSON.stringify(location.coords.longitude.toFixed(4))}`);

            })();
        }, [navigation])
    );

    useCallback(() => {
        setText(errorMsg);
    }, [errorMsg]);

    //Pour la demo : les cartes ont des latitude et longitude données manuellement et nous avons pris les coordonnées de la position GPS pour notre carte.
    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_OSMDROID}
                ref={mapRef => mapRef === null ? null : mapRef.fitToElements(true)}
                style={isMapReady ? styles.map : {}}
                initialRegion={region}
                showsUserLocation={true}
                onMapReady={handleMapReady}
                maxZoomLevel={20}
            >
                <UrlTile
                    urlTemplate="https://a.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    maximumZ={20}
                />
                {devices.map(device => (
                    <Marker
                        key={String(device.id)}
                        coordinate={{ latitude: Number(device.latitude), longitude: Number(device.longitude) }}
                        title={device.nom}
                        description={`latitude: ${device.latitude}, longitude: ${device.longitude}`}
                        pinColor={device.id == 0 ? 'blue' : 'green'}
                    />
                ))}
            </MapView>
            <View style={styles.buttonContainer}>
                <View style={styles.bubble}>
                    <Text>Moi : {text}</Text>
                </View>
            </View>
        </View>
    );
}

export default MapScreen;

