import React from "react";
import { View } from 'react-native';
import { Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import BluetoothSerial from "react-native-bluetooth-serial";
import { styles } from '../../styles';

function BluetoothActivationScreen() {
    BluetoothSerial.requestEnable();
    return (
        <View style={styles.layout}>
            <Text style={styles.text} > Merci d'activer le Bluetooth pour pouvoir utiliser l'application.</Text>
        </View>
    );
}

export default BluetoothActivationScreen;