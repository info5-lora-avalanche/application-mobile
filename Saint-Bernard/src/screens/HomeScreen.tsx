import React, { useCallback, useEffect, useState } from "react";
import { Image, ScrollView, View } from 'react-native';
import { Text, IconButton, Colors, List } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import BluetoothSerial from "react-native-bluetooth-serial";
import { styles } from '../../styles';
import { useDatabaseConnection } from '../data/connection';
import { useFocusEffect } from '@react-navigation/native';
import { DeviceModel } from "../data/entities/DeviceModel";

function HomeScreen({ navigation }) {
    const [msgs, setMsgs] = useState<DeviceModel[]>([]);
    const { devicesRepository } = useDatabaseConnection();

    useFocusEffect(
        useCallback(() => {
            (async () => {
                await devicesRepository.getAllFriends().then((res) => {
                    setMsgs(res.filter(device => device.signal < -10 ))
                });
            })();
        }, [navigation])
      );

    return (
        <View style={styles.layout}>
            <Image
                style={styles.logo}
                source={require('../assets/adaptive-icon.png')}
            />
            <Text style={styles.text} > {<Icon style={styles.icon} name='alert-circle-outline' />} Notifications</Text>
            <ScrollView
                style={styles.layout}
            >
                {msgs.map(msg => (
                    <List.Item
                        key={msg.mac}
                        title={`${msg.nom} est en danger !`}
                    />
                ))}
            </ScrollView>
            <View style={styles.iconButton}>
                <IconButton
                    icon="qrcode-scan"
                    color={Colors.red500}
                    size={40}
                    onPress={() => navigation.navigate('QRCode', { screen: 'Scanner', params: { isFriend: true }})}
                />
            </View>
        </View>
    );
}

export default HomeScreen;


