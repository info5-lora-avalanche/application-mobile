import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('devices')
export class DeviceModel {
  @PrimaryGeneratedColumn('increment')
  id: number;

  @Column()
  nom: string;

  @Column({unique: true})
  mac: string;

  @Column({type: "varchar", length: 4})
  code: string;

  @Column({nullable: true})
  commentaire: string;

  @Column({nullable: true})
  latitude: number;

  @Column({nullable: true})
  longitude: number;

  @Column({nullable: true})
  signal: number;
}