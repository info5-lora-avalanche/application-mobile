import React, {
    createContext,
    useCallback,
    useContext,
    useEffect,
    useState,
  } from 'react';
  import { ActivityIndicator } from 'react-native';
  import { Connection, createConnection } from 'typeorm';
  
  import { DeviceModel } from './entities/DeviceModel';
  import { DevicesRepository } from './repositories/DevicesRepository';
  
  interface DatabaseConnectionContextData {
    devicesRepository: DevicesRepository;
  }
  
  const DatabaseConnectionContext = createContext<DatabaseConnectionContextData>(
    {} as DatabaseConnectionContextData,
  );
  
  export const DatabaseConnectionProvider: React.FC = ({ children }) => {
    const [connection, setConnection] = useState<Connection | null>(null);
  
    const connect = useCallback(async () => {
      const createdConnection = await createConnection({
        type: 'expo',
        database: 'database',
        driver: require('expo-sqlite'),
        entities: [DeviceModel],
        synchronize: true,
      });
  
      setConnection(createdConnection);
    }, []);
  
    useEffect(() => {
      if (!connection) {
        connect();
      }
    }, [connect, connection]);
  
    if (!connection) {
      return <ActivityIndicator />;
    }
  
    return (
      <DatabaseConnectionContext.Provider
        value={{
          devicesRepository: new DevicesRepository(connection),
        }}
      >
        {children}
      </DatabaseConnectionContext.Provider>
    );
  };
  
  export function useDatabaseConnection() {
    const context = useContext(DatabaseConnectionContext);
  
    return context;
  }
  