import { Connection, Repository } from 'typeorm';
import { DeviceModel } from '../entities/DeviceModel';
import * as Location from 'expo-location';
import { LocationObject } from 'expo-location';

interface ICreateDeviceData {
  nom: string;
  mac: string;
  code: string;
  commentaire?: string;
  latitude?: number;
  longitude?: number;
  signal?: number;
}

export class DevicesRepository {
  private ormRepository: Repository<DeviceModel>;

  constructor(connection: Connection) {
    this.ormRepository = connection.getRepository(DeviceModel);
  }

  /**
   * Permet de récupérer tous les appareils enregistrés dans la base de données
   */
  public async getAll(): Promise<DeviceModel[]> {
    const devices = await this.ormRepository.find();

    return devices;
  }

  public async deleteAll() {
    const devices = await this.ormRepository.delete({});

  }

  /**
   * Permet de récupérer la carte de l'utilisateur de l'application
   */
  public async getMyDevice(): Promise<DeviceModel | undefined> {
    const device = await this.ormRepository.findOne(0);

    return device;
  }

  /**
 * Permet de renommer un appareil enregistré dans la base de données
 * @param id : id de l'appareil à renommer
 * @param nom : nouveau nom de l'appareil
 */
  public async saveMyDevice({ nom, mac, code, commentaire }: ICreateDeviceData): Promise<void> {
    let { status } = await Location.requestPermissionsAsync();
    if (status !== 'granted') {
      console.log('Permission to access location was denied');
      return;
    }
    let location: LocationObject = await Location.getCurrentPositionAsync(undefined).catch(console.error);
    let latitude = location.coords.latitude.toFixed(4);
    let longitude = location.coords.longitude.toFixed(4);
    await this.ormRepository.query(
      `
        INSERT INTO
          devices
          (
            id,
            nom,
            mac,
            code,
            commentaire,
            latitude,
            longitude
          )
        VALUES
        (
          0,
          ?,
          ?,
          ?,
          ?,
          ?,
          ?
        );
        `,
      [nom, mac, code, commentaire, latitude, longitude],
    );
  }

  /**
   * Permet de récupérer tous les appareils enregistrés dans la base de données
   * sauf le notre
   */
  public async getAllFriends(): Promise<DeviceModel[]> {
    const devices = await this.ormRepository.query(
      `
        SELECT
          *
        FROM
          devices
        WHERE
          id != 0;
        `,
      [],
    );

    return devices;
  }

  /**
   * Permet d'enregistrer un nouvel appareil dans la base de données
   * @param param0 : Objet représentant l'appareil à enregistrer
   */
  public async create({ nom, mac, code, commentaire, latitude, longitude }: ICreateDeviceData): Promise<DeviceModel> {
    const device = this.ormRepository.create({
      nom,
      mac,
      code,
      commentaire,
      latitude,
      longitude,
    });

    await this.ormRepository.save(device);

    return device;
  }

  /**
   * Permet de renommer un appareil enregistré dans la base de données
   * @param id : id de l'appareil à renommer
   * @param nom : nouveau nom de l'appareil
   */
  public async rename(id: number, nom: string, commentaire: string): Promise<void> {
    await this.ormRepository.query(
      `
      UPDATE
        devices
      SET
        nom = ?,
        commentaire = ?
      WHERE
        id = ?;
      `,
      [nom, commentaire, id],
    );
  }

  /**
   * Permet de mettre à jour la localisation d'une carte
   */
  public async updateLocation(id: number, latitude: number, longitude: number): Promise<void> {
    await this.ormRepository.query(
      `
      UPDATE
        devices
      SET
        latitude = ?,
        longitude = ?
      WHERE
        id = ?;
      `,
      [latitude, longitude, id],
    );
  }

    /**
   * Permet de mettre à jour une carte
   */
     public async updateInfo(mac: string, latitude: number, longitude: number, signal: number): Promise<void> {
      await this.ormRepository.query(
        `
        UPDATE
          devices
        SET
          latitude = ?,
          longitude = ?,
          signal = ?
        WHERE
          mac = ?;
        `,
        [latitude, longitude, signal, mac],
      );
    }

  /**
   * Permet de supprimer un appareil de la base de données
   * @param id : id de l'appareil à supprimer
   */
  public async delete(id: number): Promise<void> {
    await this.ormRepository.delete(id);
  }
}
