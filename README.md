# Project Saint-Bernard : Part for the mobile application
_To see further informations about the project go to the [Documentation repository](https://gitlab.com/info5-lora-avalanche/documentation)._

## Introduction

In France, each year we count an average of 15,000 requests for mountain rescue. In Isère, the average number of mountain rescues per year is 400. Mountain rescue is divided between several services and associations: Peloton de Gendarmerie de Haute Montagne (PGHM), CRS Alpes, Groupement de Montagne des Sapeurs Pompiers (GMSP), Association Nationale des Médecins et Sauveteurs en Montagne (ANMSM) and the SAMU.

The greatest difficulty in mountain rescue is locating the victim. Sometimes, the very fact of knowing that a person needs to be rescued is a real problem, especially in white zones.

Our project, named Saint-Bernard and formerly called LoRa-valanche, aims to address both of these issues.

#### Our goals
We want to improve the rescue success rate by making a tool which is more accessible, effective and efficient.

We want this solution to be:
* **simple:** usable by all types of users
* **precise:** more precise than an avalanche transceiver
* **affordable:** a solution at reduced cost

#### For whom ?
Our work is done  in patrnership with the Peloton de Gendarmerie de Haute Montagne.
Our tool is made for:
* **rescuers** as a complement to an avalanche transceiver
* **people going** to the mountains who are potential victims.

#### How does it work ?
This tool makes it possible to send, at a predefined frequency, the user's GPS coordinates to a remote server. The transmission is done through the mobile network (3g & 4g). The limit of this solution is of course the white zones in the mountains.


## Presentation of the Mobile App
The app for this project has been totally redone. The former app is available [here](https://gitlab.com/info5-lora-avalanche/application-mobile-old).

The mobile app is needed to display the different chips on a map.
This version of the mobile app implements the following features :
  - Reading and generating QR-Codes
  - Storing devices
  - Receiving Bluetooth messages from **our** chip
  - Location of chips that are saved in the app
  - Display the chips on a map

> * Technical documentation available [here]()
> * A wiki is also available [here]()

## Technologies used :
* React native
* Expo
> See the dependencies [here](https://gitlab.com/info5-lora-avalanche/application-mobile/-/blob/dev/Saint-Bernard/package.json)

## Releases :
